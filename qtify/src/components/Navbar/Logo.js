import logo from "../../assets/logo.png";

const Logo = () => {
    return (
        <div>
        <img src={logo} alt='Logo' className="img" />
        </div>
    )
};

export default Logo;